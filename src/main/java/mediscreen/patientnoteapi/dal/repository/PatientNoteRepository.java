package mediscreen.patientnoteapi.dal.repository;

import mediscreen.patientnoteapi.dal.document.PatientNoteDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientNoteRepository extends MongoRepository<PatientNoteDocument, String> {
    List<PatientNoteDocument> findByPatientId(Integer patientId);
}
