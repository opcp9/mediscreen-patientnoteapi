package mediscreen.patientnoteapi.dal.document;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "patientnote")
public class PatientNoteDocument {

    @Id
    private String id;
    private Integer patientId;
    private String patientNote;
}
