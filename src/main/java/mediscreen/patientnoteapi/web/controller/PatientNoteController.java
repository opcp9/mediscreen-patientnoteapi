package mediscreen.patientnoteapi.web.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mediscreen.patientnoteapi.dal.document.PatientNoteDocument;
import mediscreen.patientnoteapi.domain.service.PatientNoteService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.NoSuchElementException;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/patientnote")
public class PatientNoteController {

    private final PatientNoteService patientNoteService;

    @GetMapping("/patient/{patientid}")
    public List<PatientNoteDocument> getPatientNotesByPatientId(@PathVariable Integer patientid) {
        log.debug("request for getting all patientNotes for patientid={}", patientid);

        return patientNoteService.getPatientNotesByPatientId(patientid);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPatientNoteById(@PathVariable String id) {
        log.debug("request for getting patientNote with id={}", id);

        try {
            PatientNoteDocument patientNoteDocument = patientNoteService.getPatientNoteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(patientNoteDocument);
        } catch (NoSuchElementException e) {
            String logAndBodyMessage = "error while getting patientNote because of missing patientNote with id=" + id;
            log.error(logAndBodyMessage);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(logAndBodyMessage);
        }
    }

    @PostMapping()
    public ResponseEntity<?> postPatientNote(@RequestBody PatientNoteDocument patientNoteDocument) {
        log.debug("request for posting patientNote with patientid={}", patientNoteDocument.getPatientId());

        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(patientNoteService.createPatientNote(patientNoteDocument));
        } catch (NoSuchElementException e) {
            String logAndBodyMessage = "error while posting patientNote with null patientId";
            log.error(logAndBodyMessage);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(logAndBodyMessage);
        }

    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putPatientNote(@PathVariable String id,
                                            @RequestBody PatientNoteDocument patientNoteDocument) {
        log.debug("request for putting patientNote with id={}", id);

        try {
            patientNoteDocument.setId(id);
            return ResponseEntity.status(HttpStatus.OK).body(patientNoteService.updatePatientNote(patientNoteDocument));
        } catch (NoSuchElementException e) {
            String logAndBodyMessage = "error while putting patientNote because missing patientNote with id=" + id;
            log.error(logAndBodyMessage);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(logAndBodyMessage);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePatientNoteById(@PathVariable String id) {
        log.debug("request for deleting patientNote with id={}", id);

        try {
            patientNoteService.deletePatientNoteById(id);
            return ResponseEntity.status(HttpStatus.OK).body("successfully delete patientNote with id=" + id);
        } catch (NoSuchElementException e) {
            String logAndBodyMessage = "error while deleting patientNote because of missing patientNote with id=" + id;
            log.error(logAndBodyMessage);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(logAndBodyMessage);
        }
    }

}
