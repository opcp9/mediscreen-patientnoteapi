package mediscreen.patientnoteapi.domain.service;

import lombok.AllArgsConstructor;
import mediscreen.patientnoteapi.dal.document.PatientNoteDocument;
import mediscreen.patientnoteapi.dal.repository.PatientNoteRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@AllArgsConstructor
@Service
public class PatientNoteService {

    private final PatientNoteRepository patientNoteRepository;

    public List<PatientNoteDocument> getPatientNotesByPatientId(Integer patientId) {
        return patientNoteRepository.findByPatientId(patientId);
    }

    public PatientNoteDocument getPatientNoteById(String id) throws NoSuchElementException {
        return patientNoteRepository.findById(id)
                .orElseThrow(NoSuchElementException::new);
    }

    public PatientNoteDocument createPatientNote(PatientNoteDocument patientNoteDocument) {
        if (null == patientNoteDocument.getPatientId()) {
            throw new NoSuchElementException();
        }
        return patientNoteRepository.insert(patientNoteDocument);
    }

    public PatientNoteDocument updatePatientNote(PatientNoteDocument patientNoteDocument) {
        patientNoteRepository.findById(patientNoteDocument.getId())
                .orElseThrow(NoSuchElementException::new);
        return patientNoteRepository.save(patientNoteDocument);
    }

    public void deletePatientNoteById(String id) {
        patientNoteRepository.findById(id)
                .orElseThrow(NoSuchElementException::new);
        patientNoteRepository.deleteById(id);
    }
}
