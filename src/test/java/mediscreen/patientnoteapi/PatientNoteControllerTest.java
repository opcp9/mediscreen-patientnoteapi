package mediscreen.patientnoteapi;

import mediscreen.patientnoteapi.dal.document.PatientNoteDocument;
import mediscreen.patientnoteapi.domain.service.PatientNoteService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.NoSuchElementException;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class PatientNoteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PatientNoteService patientNoteService;

    @Test
    public void should_returnRightPatientNoteList_whenGetPatientNotesByPatientId() throws Exception {
        when(patientNoteService.getPatientNotesByPatientId(anyInt())).thenReturn(Collections.singletonList(getFakePatientNoteDocument()));

        mockMvc.perform(get("/patientnote/patient/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(String.valueOf(getFakePatientNoteDocument().getPatientNote()))));
    }

    @Test
    public void should_returnRightPatientNote_whenGetPatientNoteById() throws Exception {
        when(patientNoteService.getPatientNoteById(anyString())).thenReturn(getFakePatientNoteDocument());

        mockMvc.perform(get("/patientnote/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(String.valueOf(getFakePatientNoteDocument().getPatientNote()))));
    }

    @Test
    public void should_returnNotFound_whenGetMissingPatientNoteById() throws Exception {
        when(patientNoteService.getPatientNoteById(anyString())).thenThrow(NoSuchElementException.class);

        mockMvc.perform(get("/patientnote/1"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("error")));
    }

    @Test
    public void should_createPatientNote_whenPostNewPatientNote() throws Exception {
        when(patientNoteService.createPatientNote(any())).thenReturn(getFakePatientNoteDocument());
        String inputJson = "{\"patientId\": 1,\"patientNote\": \"patientNoteTest\"}";

        mockMvc.perform(post("/patientnote")
                        .content(inputJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void should_notCreatePatientNote_whenPostNewPatientNoteWithNullPatientId() throws Exception {
        when(patientNoteService.createPatientNote(any())).thenThrow(NoSuchElementException.class);
        String inputJson = "{\"patientId\": null,\"patientNote\": \"patientNoteTest\"}";

        mockMvc.perform(post("/patientnote")
                        .content(inputJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void should_updatePatientNote_whenPutExistingPatientNote() throws Exception {
        when(patientNoteService.updatePatientNote(any(PatientNoteDocument.class))).thenReturn(getFakePatientNoteDocument());
        String inputJson = "{\"patientId\": 1,\"patientNote\": \"patientNoteTest\"}";

        mockMvc.perform(put("/patientnote/1")
                        .content(inputJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void should_notUpdatePatientNote_whenPutMissingPatientNote() throws Exception {
        when(patientNoteService.updatePatientNote(any(PatientNoteDocument.class))).thenThrow(NoSuchElementException.class);
        String inputJson = "{\"patientId\": 1,\"patientNote\": \"patientNoteTest\"}";

        mockMvc.perform(put("/patientnote/1")
                        .content(inputJson)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void should_deletePatientNote_whenDeleteExistingPatientNote() throws Exception {
        mockMvc.perform(delete("/patientnote/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void should_NotDeletePatientNote_whenDeleteMissingPatientNote() throws Exception {
        doThrow(new NoSuchElementException()).when(patientNoteService).deletePatientNoteById(anyString());

        mockMvc.perform(delete("/patientnote/1"))
                .andExpect(status().isNotFound());
    }

    private PatientNoteDocument getFakePatientNoteDocument() {
        PatientNoteDocument patientNoteDocument = new PatientNoteDocument();
        patientNoteDocument.setId("1");
        patientNoteDocument.setPatientId(1);
        patientNoteDocument.setPatientNote("patientNoteTest");
        return patientNoteDocument;
    }
}
