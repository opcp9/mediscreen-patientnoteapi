package mediscreen.patientnoteapi;

import mediscreen.patientnoteapi.dal.document.PatientNoteDocument;
import mediscreen.patientnoteapi.dal.repository.PatientNoteRepository;
import mediscreen.patientnoteapi.domain.service.PatientNoteService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PatientNoteServiceTest {

    @InjectMocks
    private PatientNoteService patientNoteService;

    @Mock
    private PatientNoteRepository patientNoteRepository;

    @Test
    public void should_returnSomething_whenGetPatientNotesByPatientId() {
        when(patientNoteRepository.findByPatientId(anyInt())).thenReturn(Collections.singletonList(getFakePatientNoteDocument()));

        assertThat(patientNoteService.getPatientNotesByPatientId(anyInt())).isNotNull();
    }

    @Test
    public void should_findPatientNote_whenGetExistingPatientNoteById() {
        when(patientNoteRepository.findById(anyString())).thenReturn(Optional.of(getFakePatientNoteDocument()));

        PatientNoteDocument patientNoteDocument = patientNoteService.getPatientNoteById(anyString());

        assertThat(patientNoteDocument).isNotNull();
        verify(patientNoteRepository, times(1)).findById(anyString());
    }

    @Test
    public void should_throwNoSuchElementException_whenGetExistingPatientNoteById() {
        when(patientNoteRepository.findById(anyString())).thenReturn(Optional.empty());

        assertThatExceptionOfType(NoSuchElementException.class)
                .isThrownBy(() -> patientNoteService.getPatientNoteById(anyString()));
    }

    @Test
    public void should_insertPatientNote_whenCreateNewPatientNote() {
        when(patientNoteRepository.insert(any(PatientNoteDocument.class))).thenReturn(getFakePatientNoteDocument());

        PatientNoteDocument patientNoteDocument = patientNoteService.createPatientNote(getFakePatientNoteDocument());

        assertThat(patientNoteDocument).isNotNull();
        verify(patientNoteRepository, times(1)).insert(any(PatientNoteDocument.class));
    }

    @Test
    public void should_throwNoSuchElementException_whenCreatePatientNoteWithNullPatientId() {
        PatientNoteDocument patientNoteDocument = getFakePatientNoteDocument();
        patientNoteDocument.setPatientId(null);

        assertThatExceptionOfType(NoSuchElementException.class)
                .isThrownBy(() -> patientNoteService.createPatientNote(patientNoteDocument));
    }

    @Test
    public void should_savePatientNote_whenUpdateExistingPatientNote() {
        when(patientNoteRepository.findById(anyString())).thenReturn(Optional.of(getFakePatientNoteDocument()));
        when(patientNoteRepository.save(any(PatientNoteDocument.class))).thenReturn(getFakePatientNoteDocument());

        PatientNoteDocument patientNoteDocument = patientNoteService.updatePatientNote(getFakePatientNoteDocument());

        assertThat(patientNoteDocument).isNotNull();
        verify(patientNoteRepository, times(1)).save(any());
    }

    @Test
    public void should_throwNoSuchElementException_whenUpdateMissingPatientNote() {
        when(patientNoteRepository.findById(anyString())).thenReturn(Optional.empty());

        assertThatExceptionOfType(NoSuchElementException.class)
                .isThrownBy(() -> patientNoteService.updatePatientNote(getFakePatientNoteDocument()));
    }

    @Test
    public void should_deletePatientNote_whenDeleteExistingPatientNote() {
        when(patientNoteRepository.findById(anyString())).thenReturn(Optional.of(getFakePatientNoteDocument()));

        patientNoteService.deletePatientNoteById(anyString());

        verify(patientNoteRepository, times(1)).deleteById(anyString());
    }

    @Test
    public void should_throwNoSuchElementException_whenDeleteMissingPatientNote() {
        when(patientNoteRepository.findById(anyString())).thenReturn(Optional.empty());

        assertThatExceptionOfType(NoSuchElementException.class)
                .isThrownBy(() -> patientNoteService.deletePatientNoteById(anyString()));
    }

    private PatientNoteDocument getFakePatientNoteDocument() {
        PatientNoteDocument patientNoteDocument = new PatientNoteDocument();
        patientNoteDocument.setId("1");
        patientNoteDocument.setPatientId(1);
        patientNoteDocument.setPatientNote("x");
        return patientNoteDocument;
    }
}
